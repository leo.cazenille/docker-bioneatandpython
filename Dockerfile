FROM lcazenille/ubuntupython3andjava
MAINTAINER leo.cazenille@gmail.com

# Install python dependencies
RUN pip3 --no-cache-dir install -U numpy pandas scoop git+https://gitlab.com/leo.cazenille/qdpy.git@develop

# Download repositories
RUN git clone https://bitbucket.org/AubertKato/bioneat.git
#RUN git clone https://bitbucket.org/AubertKato/bioneat.git \
#    && cd /bioneat \
#    && git checkout SSCI2019

RUN git clone https://bitbucket.org/AubertKato/daccad.git /daccad
RUN cd /daccad; ./gradlew dist --no-build-cache
RUN cp -R /daccad/build /bioneat/

# Install bioneat
RUN cd /bioneat; ./gradlew dist --no-build-cache

# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
